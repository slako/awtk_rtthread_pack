

#include <rtthread.h>
#include <rtdevice.h> 
#include <rtconfig.h>

#ifdef PKG_USING_AWTK_DEMO

#include "base/idle.h"
#include "base/timer.h"
#include "tkc/platform.h"
#include "base/main_loop.h"
#include "base/event_queue.h"
#include "base/font_manager.h"
#include "lcd/lcd_mem_fragment.h"
#include "main_loop/main_loop_simple.h"
#include "tkc/mem.h"
#include "tkc/thread.h"


#include "rt_awtk_port.h"
 

 
extern int gui_app_start(int lcd_w, int lcd_h);

extern ret_t platform_prepare(void);


void awtk_thread(void* args) {
    //struct rt_device_graphic_info *rt_dev_grap = rt_awtk_port_get_grap_pointer();
    //gui_app_start(rt_dev_grap->width, rt_dev_grap->height);
    gui_app_start(480,480);
}


int awtk_ui_thread(void)
{
    rt_thread_t thread = RT_NULL;
    /*
    rt_awtk_port_lcd_init();
    rt_awtk_port_indev_init();

    */
    //platform_prepare();

    thread = rt_thread_create("awtk_thread", 
                              awtk_thread, 
                              RT_NULL, 
                              4096*20,
                              5, 
                              10);
    
    if (thread == RT_NULL){
        return RT_ERROR;
    }
    rt_thread_startup(thread);

    
    return RT_EOK;
}
INIT_APP_EXPORT(awtk_ui_thread);



#endif


