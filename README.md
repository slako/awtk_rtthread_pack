# AWTK Pack包使用说明

## 拷贝配置awtk的kconfig，并添加到rtthread的Kconfig

拷贝env/local_pkgs/awtk到你的环境，如果你是Macbook，拷贝到 ～/.env/local_pkgs/

编辑你环境的rt-thread的Kconfig,如果你是Macbook，编辑～/.env/packages/Kconfig，在该文件最后加上一句 source "$PKGS_DIR/../local_pkgs/awtk/Kconfig"

## 到rt-thread项目目录配置awtk

```
cd ~/GitHub/rt-thread/bsp/qemu-vexpress-a9
scons --menuconfig
source ~/.env/env.sh
pkgs --update
scons
./qemu.sh

```

### 假如在pkgs --update 不成功
```
cd ~/GitHub/rt-thread/bsp/qemu-vexpress-a9/packages/
git clone https://gitee.com/slako/awtk_rtthread_pack.git awtk-latest
cd awti-latest
git submodule update --init


```
